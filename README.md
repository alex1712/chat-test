# Chat Challenge #

Explanation of the challenge:

Implementation of Chat Functionality

We like to give folks some fun challenges that are actually close to our real domain, and meaningful contact between two people is a cornerstone of our platform.
So, we'd like to see a super simple chat service and a very basic front end using it.

We are currently using node and Java for the backend. The choice of framework is up to yourself; we'd much rather see examples of clean well tested code, as opposed to you struggling with a potentially new framework. 
If you really want to use our tech, we are using Spring boot for services, and our current front-end is angular for web, swift for iOS, and react for Android: though don't do all of them, just choose one!

The design of the solution is completely up to you.
However, as this is a coding exercise, if you say 'download ejabberd' or copy the spring boot chat example, this doesn't really help us assess your coding (smile). 
Extra points given for thinking of scalability (for example, certain technologies might be easier to load balance, or run at scale).

Also, please bear in mind that it needs to be quite changeable in the future, as the Product Owner has big plans in this space!
We need to be able to send and receive messages between two people. 
We don't really need to see any conversation history, or even a list of chats between people. If you feel like showing a little extra, however, feel free. 
Technical Guidance

We wouldn't ask anyone to do a coding puzzle that we haven't done ourselves, so the people looking at your code understand the problem we're asking to be solved.

We value clean, readable automated testing, so please demonstrate this on the abstraction levels you deem appropriate (either unit and / or integration).
We also value clean code principles (such as SOLID etc)
Some build / make / run instructions should be included with the code
If you use any servers / services please include a deployment script, or setup instructions for them
We also like to see your ‘long division’ i.e. the working out of the problem in code, so use a git repository and add commits for significant working steps. Either send your git repo with the code, or push it to github and give us access
Extra nice to see is a little bit of TDD


### Introduction ###

As said in the challenge the solution should be really changeable in the future, therefor my solutions is not 
limiting the chat to only 2 persons but it is a generic chat service from which you would be able to easily build a two persons chat or 
a complete rooms system chat. In fact, my goal was to do the chat service so that two users could open a private room and chat privately there.
But I couldn't accomplished that in time.

Anyway, I will try to explain the design and how the application works.

As you wrote that thinking about scalability is important for you I designed the following system

![Design image](https://dl.dropboxusercontent.com/s/sdiwoluloap4wdg/IMG_20170402_111623.jpg?dl=0)

My idea is that with this design the API is fully scalable as you can just add more nodes and they will make only 1 connection 
to the dispatcher for all the users connected to this node. Right now this architecture is completely exagerated, but the benefit of it is 
that in the future, the dispatcher could also be scalable by making different dispatchers for different contexts like chat rooms or geographical and leaving the api
the ability to connect only to the dispatchers it needs.

### Setup ###

First of all you need to have `nodejs` and `npm installed` in your computer.

Once you have this 2 requisites, there is a Makefile to facilitate the installation of all npm modules in the
different submodules. So from the root folder of the project you can run `make installNPM`

### Run ###

As the system is divided in 3 applications, 2 node servers and 1 web client you would need to run the 3 projects. 

For the servers I did a server wrapper which will run both servers together, to run it just open a terminal in the project folder
 and type `cd server && npm start`
 
For the client, which is a react project from a react seed, you will have to open a second terminal and
type from the project folder `cd chat-client && npm start`

That's all, the application should be automatically open in your browser. In the case that that's not working, please 
don't hesitate to contact me, it can be taht I have something from npm installed globally and was not in some of the package.json files.

### Test ###

As I comment in the TODO'S list the tests are right now and only for time limit reasons only in the dispatcher server.
Yout can run them by typing from the project root folder `cd server/chat-dispatcher && npm test`


### TODO'S ###

- The constants should be an extra node module to be able to share the constants.
- There should be a common model (protocol) for the events which all modules should share to ensure the format of the 
 data.
- For a time limit reason the tests are now limited to the dispatcher. It would be urgent in a project to apply tests 
at least in the client. As the API is only forwarding, for now is less critical, but as soon
as the API does also mapping to right dispatcher and maybe security checks should be also critical to 
add tests there.
- Validations is a critical part which I didn't do, that is really an issue and sadly I didn't manage to do it. I disabled in the
client the possibility to send empty messages or to join with empty nicknames though.
- Also, I would implement flow to check types and use immutable.js for all data models, with flow we would avoid the most 
of type errors and with immutability we would avoid side effects errors.
