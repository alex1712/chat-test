.PHONY: cleanNPM

cleanNPM:
	if [ -d "chat-client/node_modules" ] ; \
    then \
         cd chat-client && rm -r node_modules ; \
    fi;
	if [ -d "server/node_modules" ] ; \
    then \
         cd server && rm -r node_modules ; \
    fi;
	if [ -d "server/chat-api/node_modules" ] ; \
    then \
         cd server/chat-api && rm -r node_modules ; \
    fi;
	if [ -d "server/chat-dispatcher/node_modules" ] ; \
    then \
         cd server/chat-dispatcher && rm -r node_modules ; \
    fi;
installNPM:
	cd chat-client && npm install
	cd server && npm install
	cd server/chat-api && npm install
	cd server/chat-dispatcher && npm install