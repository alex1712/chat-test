import ChatActionTypes from './ChatActionTypes';
import ChatAPI from '../api/ChatAPI';
import ChatDispatcher from '../dispatcher/ChatDispatcher';
const {SocketEventTypes} = require('../../../constants/Events');


ChatAPI.client.on(SocketEventTypes.user_joined, (data) => {
    ChatDispatcher.dispatch({
        type: ChatActionTypes.USER_JOINED,
        data
    });
});
ChatAPI.client.on(SocketEventTypes.user_left, (data) => {
    ChatDispatcher.dispatch({
        type: ChatActionTypes.USER_LEFT,
        data
    });
});
ChatAPI.client.on(SocketEventTypes.message_sent, (data) => {
    ChatDispatcher.dispatch({
        type: ChatActionTypes.MESSAGE_SENT,
        data
    });
});

export default {
    setNickname(nickname) {
        const dispatchSelfUserJoined = ({user}) => {
            if(nickname === user) {
                ChatDispatcher.dispatch({
                    type: ChatActionTypes.SELF_JOINED,
                    data: nickname
                });
                ChatAPI.client.off(SocketEventTypes.user_joined, dispatchSelfUserJoined);
            }
        };
        ChatAPI.client.on(SocketEventTypes.user_joined, dispatchSelfUserJoined);
        ChatAPI.setNickname(nickname);
    },
    sendMessage(message) {
        ChatAPI.sendMessage(message);
    }
};