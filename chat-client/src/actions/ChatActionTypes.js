import keymirror from 'keymirror';


const ActionTypes = keymirror({
    SELF_JOINED: null,
    USER_JOINED: null,
    USER_LEFT: null,
    MESSAGE_SENT: null
});

export default ActionTypes;
