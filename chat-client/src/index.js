import React from 'react';
import ReactDOM from 'react-dom';
import ChatContainer from './containers/ChatContainer';
import './index.css';

ReactDOM.render(
  <ChatContainer />,
  document.getElementById('root')
);
