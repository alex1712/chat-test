const io = require('socket.io-client');
const APIConfig = require('../../config/APIConfig');
const {SocketEventTypes} = require('../../../constants/Events');

const apiURL = `http://${APIConfig.host}:${APIConfig.port}`
const DispatcherConnectionOpt ={
    transports: ['websocket'],
    'force new connection': true
};

const client = io.connect(apiURL, DispatcherConnectionOpt);
export default {
    client: client,
    setNickname(nickname) {
        client.emit(SocketEventTypes.join_user, nickname);
    },
    sendMessage(message) {
        client.emit(SocketEventTypes.message_sent, message);
    }
}