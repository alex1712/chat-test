import {ReduceStore} from 'flux/utils';
import ChatActionTypes from '../actions/ChatActionTypes';
import ChatDispatcher from '../dispatcher/ChatDispatcher';

class SelfUserStore extends ReduceStore {
    constructor() {
        super(ChatDispatcher);
    }

    getInitialState() {
        return null;
    }

    reduce(state, action) {
        switch (action.type) {
            case ChatActionTypes.SELF_JOINED:
                return action.data;
            default:
                return state;
        }
    }
}
export default new SelfUserStore();