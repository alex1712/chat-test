import {ReduceStore} from 'flux/utils';
import ChatActionTypes from '../actions/ChatActionTypes';
import ChatDispatcher from '../dispatcher/ChatDispatcher';

class TodoDraftStore extends ReduceStore {
    constructor() {
        super(ChatDispatcher);
    }

    getInitialState() {
        return [];
    }

    reduce(state, action) {
        switch (action.type) {
            case ChatActionTypes.MESSAGE_SENT:
                return state.concat(action.data.message);
            default:
                return state;
        }
    }
}

export default new TodoDraftStore();
