import {ReduceStore} from 'flux/utils';
import ChatActionTypes from '../actions/ChatActionTypes';
import ChatDispatcher from '../dispatcher/ChatDispatcher';

class UsersStore extends ReduceStore {
    constructor() {
        super(ChatDispatcher);
    }

    getInitialState() {
        return [];
    }

    reduce(state, action) {
        switch (action.type) {
            case ChatActionTypes.USER_JOINED:
            case ChatActionTypes.USER_LEFT:
                return action.data.users.slice(0);
            default:
                return state;
        }
    }
}

export default new UsersStore();
