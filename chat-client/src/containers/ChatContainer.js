import React, {Component} from 'react';
import ChatActions from '../actions/ChatActions';
import MessagesStore from '../stores/MessagesStore';
import UsersStore from '../stores/UsersStore';
import SelfUserStore from '../stores/SelfUserStore';
import ChatView from '../views/ChatView';

import {Container} from 'flux/utils';

class ChatContainer extends Component {
    static getStores() {
        return [
            MessagesStore,
            UsersStore,
            SelfUserStore
        ];
    }

    static calculateState() {
        return {
            messages: MessagesStore.getState(),
            users: UsersStore.getState(),
            myself: SelfUserStore.getState(),
            onSetNickname: ChatActions.setNickname
        };
    }

    render() {
        return <ChatView {...this.state} />;
    }
}

export default Container.create(ChatContainer);