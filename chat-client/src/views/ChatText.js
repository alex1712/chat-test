import React, { Component } from 'react';
import Message from './Message';
import './ChatView.css';

export default class ChatText extends Component {

    componentDidUpdate() {
        this.wrapper.scrollTop = this.wrapper.scrollHeight - this.wrapper.clientHeight;
    };
    render() {
        return (
            <div
                className="Chat-text"
                ref={(wrapper) => { this.wrapper= wrapper; }}
            >
                {this.props.messages.map((m, i) => <Message key={i} message={m} />)}
            </div>
        );
    }
}