import React, { Component } from 'react';
import {ListItem} from 'material-ui/List';
import './ChatView.css';


export default class User extends Component {
    render() {
        return (
            <ListItem>{this.props.user}</ListItem>
        );
    }
}