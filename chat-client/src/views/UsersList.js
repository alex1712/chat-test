import React, { Component } from 'react';
import {List} from 'material-ui/List';
import User from './User';
import './ChatView.css';

export default class UsersList extends Component {
render() {
        return (
            <aside className="User-list">
                <List>
                    {this.props.users.map(u => <User key={u} user={u}/>)}
                </List>
            </aside>
        );
    }
}
//