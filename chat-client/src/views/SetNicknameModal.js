import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import ChatActions from '../actions/ChatActions';

export default class SetNicknameModal extends Component {
    state = {
        nickname: ''
    };

    componentDidMount = () => {
        this.focus();
    };
    handleSubmit = () => {
        if (!this.isNicknameEmpty()) ChatActions.setNickname(this.state.nickname);
    };
    handleNickChange = (ev) => {
        this.setState({nickname: ev.currentTarget.value});
    };
    handleKeyPress = (ev) => {
        if(ev.key === 'Enter') {
            this.handleSubmit();
            ev.preventDefault();
        }
    };
    focus() {
        this.textInput.focus();
    };
    isNicknameEmpty() {
        return this.state.nickname.trim().length === 0;
    };

    render() {
        const actions = [
            <FlatButton
                label="Submit"
                primary={true}
                disabled={this.isNicknameEmpty()}
                onClick={this.handleSubmit}
            />,
        ];

        return (
            <div>
                <Dialog
                    title="Choose a nickname to join"
                    actions={actions}
                    modal={true}
                    open={this.props.open}
                >
                    <TextField
                        ref={(input) => { this.textInput = input; }}
                        onChange={this.handleNickChange}
                        onKeyPress={this.handleKeyPress}
                        hintText="Type here your nickname"
                    />
                </Dialog>
            </div>
        );
    }
}