import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentSend from 'material-ui/svg-icons/content/send';
import ChatActions from '../actions/ChatActions';
import './ChatView.css';

const inputStyle = {
    marginLeft: '1em',
    marginRight: '1em'
    // ,
    // width: 'calc(100% - 2em)'
};
const buttonStyle = {
    float: 'right',
    position: 'absolute',
    right: '10px'
};

export default class ChatInput extends Component {

    state = {
        message: ''
    };
    componentDidUpdate = () => {
        this.focus();
    };
    handleMessageSubmit = () => {
        if (!this.isMessageEmpty()) {
            ChatActions.sendMessage({
                author: this.props.user,
                text: this.state.message
            });
            this.setState({message: ''});
        }
    };
    handleMessageChange = (ev) => {
        this.setState({message: ev.currentTarget.value});
    };
    handleKeyPress = (ev) => {
        if(ev.key === 'Enter') {
            ev.preventDefault();
            this.handleMessageSubmit();
        }
    };
    focus() {
        this.textInput.focus();
    };
    isMessageEmpty() {
        return this.state.message.trim().length === 0
    };

    render() {
        return (
            <div className="Chat-input-wrapper">
                <TextField
                    ref={(input) => { this.textInput = input; }}
                    className="Chat-input"
                    hintStyle={inputStyle}
                    inputStyle={inputStyle}
                    onChange={this.handleMessageChange}
                    onKeyPress={this.handleKeyPress}
                    value={this.state.message}
                    hintText="Type here your messages..."
                    fullWidth={true}
                />
                <FloatingActionButton
                    style={buttonStyle}
                    mini={true}
                    disabled={this.isMessageEmpty()}
                    onClick={this.handleMessageSubmit}
                >
                    <ContentSend />
                </FloatingActionButton>
            </div>
        );
    }
}
