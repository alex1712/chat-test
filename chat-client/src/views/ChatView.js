import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import UsersList from './UsersList';
import ChatText from './ChatText';
import ChatInput from './ChatInput';
import SetNicknameModal from './SetNicknameModal';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './ChatView.css';
injectTapEventPlugin();

class ChatView extends Component {

  render() {
    return (
      <MuiThemeProvider>
        <div className="Chat">
          <SetNicknameModal open={!this.props.myself} />
          <div className="Chat-header">
            <h2>Chat challenge</h2>
          </div>
          <UsersList users={this.props.users} />
          <ChatText messages={this.props.messages} />
          <ChatInput user={this.props.myself} />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default ChatView;
