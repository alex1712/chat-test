import React, { Component } from 'react';
import Chip from 'material-ui/Chip';
import './ChatView.css';


const styles = {
    margin: '1.5em',
};
const labelStyle = {
    whiteSpace: 'normal'
};

export default class Message extends Component {
    render() {
        return (
            <div className="Chat-message-wrapper">
                <Chip style={styles} labelStyle={labelStyle} className="Chat-message"> {this.props.message.text}</Chip>
                <span className="Chat-message-signature">by {this.props.message.author}</span>
            </div>
        );
    }
}