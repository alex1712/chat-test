const io = require('socket.io-client');
const chai = require('chai');
const ChatDispatcher = require('../src/ChatDispatchServer');
const ServerConfig = require('../config/ServerConfig');
const {should, expect} = chai;
const {SocketEventTypes} = require('../../../constants/Events');
should();
expect();

const socketURL = `http://${ServerConfig.host}:${ServerConfig.port}`;

const options ={
    transports: ['websocket'],
    'force new connection': true
};

const alexUser = 'Alejandro';
const swapnaUser = 'Swapna';
const affinitasCTOUser = 'CTO';
const affinitasCEO = 'CEO';

const client1 = io.connect(socketURL, options);
const client2 = io.connect(socketURL, options);

const connectUser = function(user, client = client1) {
    client.emit(SocketEventTypes.join_user, user);
};
const userLeave = function(user, client = client1) {
    client.emit(SocketEventTypes.leave_user, user);
};

const userAssertion = function(expectedUser, user) {
    user.should.equal(expectedUser);
};

describe("Chat Dispatcher central server",function(){
    before(ChatDispatcher.start);
    after(ChatDispatcher.stop);

    it('Should broadcast new users joining the chat', function(done){
        connectUser(alexUser);
        client1.once(SocketEventTypes.user_joined, function({users, user}) {
            userAssertion(user, alexUser);
            expect(users.length).to.equal(1);
            connectUser(swapnaUser);
            client1.once(SocketEventTypes.user_joined, function({users, user}) {
                connectUser(affinitasCTOUser);
                expect(users.length).to.equal(2);
                userAssertion(user, swapnaUser);
            });
            client1.once(SocketEventTypes.user_joined, function({user}) {
                userAssertion(user, swapnaUser);
                client1.once(SocketEventTypes.user_joined, ({users, user}) => {
                    userAssertion(user, affinitasCTOUser);
                    expect(users.length).to.equal(3);
                    done();
                })
            });
        });
    });

    it('Should broadcast when the users leave the chat', function(done){
        client1.once(SocketEventTypes.user_left, function({users, user}) {
            expect(users.length).to.equal(2);
            userAssertion(alexUser, user);
            done();
        });
        userLeave(alexUser);
    });

    it('Should broadcast when an api node gets disconnected', function(done){
        client2.once(SocketEventTypes.user_joined, function() {
            client1.close();
            let usersCount = 2;
            client2.on(SocketEventTypes.user_left, function({users, user}) {
                expect(users.length).to.equal(usersCount);
                usersCount--;
                if(usersCount === 0) {
                    done();
                }
            });
        });
        connectUser(affinitasCEO, client2);
    });
});