const winston = require('winston');

const usersByNode = new Map();

module.exports = {
    getUsersOfNode(nodeId) { return usersByNode.has(nodeId) ? usersByNode.get(nodeId) : new Set(); },

    addUser(nodeId, user) { usersByNode.set(nodeId, this.getUsersOfNode(nodeId).add(user)); },

    deleteUser(nodeId, user) { this.getUsersOfNode(nodeId).delete(user); }
};
