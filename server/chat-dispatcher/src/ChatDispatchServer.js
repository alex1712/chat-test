const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const winston = require('winston');
const ServerConfig = require('../config/ServerConfig');
const {SocketEventTypes} = require('../../../constants/Events');

const usersByNode = require('./UsersByNode');
const allUsers = new Set();

io.on(SocketEventTypes.connection, function(socket){

    //USERS HANDLING
    winston.log('info', 'new node conection to dispatcher');
    socket.on(SocketEventTypes.join_user, function(nickname){
        winston.log('info', `user ${nickname} join the chat`);
        allUsers.add(nickname);
        usersByNode.addUser(socket.id, nickname);
        io.emit(SocketEventTypes.user_joined, {users: Array.from(allUsers.values()), user: nickname});
        // winston.log('info', `new user joined: ${JSON.stringify(user, null, 2)}`);
    });
    socket.on(SocketEventTypes.leave_user, function(nickname){
        winston.log('info', `user ${nickname} left the chat`);
        allUsers.delete(nickname);
        usersByNode.deleteUser(socket.id, nickname);
        io.emit(SocketEventTypes.user_left, {users: Array.from(allUsers.values()), user: nickname});
    });

    socket.on(SocketEventTypes.disconnect, function(){
        winston.log('warn', `node ${socket.id} disconnecting`);
        const usersToRemove = usersByNode.getUsersOfNode(socket.id);
        usersToRemove.forEach((nickname) => {
            winston.log('warn', `user ${nickname} left by disconnection of node`);
            usersByNode.deleteUser(nickname);
            allUsers.delete(nickname);
            io.emit(SocketEventTypes.user_left, {users: Array.from(allUsers.values()), user: nickname});
        });
    });

    //MESSAGES HANDLING
    socket.on(SocketEventTypes.message_sent, function(message){
        winston.log('info', `receiving message "${JSON.stringify(message, null, 2)}"`);
        io.emit(SocketEventTypes.message_sent, {message});
    });

});

module.exports = {
    start: function() {
        http.listen(ServerConfig.port, function(){
            winston.log('info', 'Dispatcher listening on *:3200');
        });
    },
    stop: function(){
        http.close();
    }
};
