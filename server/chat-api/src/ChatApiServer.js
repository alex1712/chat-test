const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ioClient = require('socket.io-client');
const winston = require('winston');
const ServerConfig = require('./../config/ServerConfig');
const DispatcherConfig = require('./../config/DispatcherConfig');
const {SocketEventTypes} = require('../../../constants/Events');

const DispatcherUrl = `http://${DispatcherConfig.host}:${DispatcherConfig.port}`;

const DispatcherConnectionOpt ={
    transports: ['websocket'],
    'force new connection': true
};
let dispatcherClient;

const addHandling = function() {
    dispatcherClient.on(SocketEventTypes.user_joined, function(usersState) {
        io.emit(SocketEventTypes.user_joined, usersState);
    });
    dispatcherClient.on(SocketEventTypes.user_left, function(usersState) {
        io.emit(SocketEventTypes.user_left, usersState);
    });
    dispatcherClient.on(SocketEventTypes.message_sent, function(newMessage) {
        io.emit(SocketEventTypes.message_sent, newMessage);
    });
};

io.on('connection', function(socket){
    winston.log('info', 'a user connected');
    let nick;
    socket.on(SocketEventTypes.disconnect, () => {
        if(nick) dispatcherClient.emit(SocketEventTypes.leave_user, nick);
    });
    socket.on(SocketEventTypes.join_user, (nickname) => {
        winston.log('info', `${nickname} joining`);
        nick = nickname;
        dispatcherClient.emit(SocketEventTypes.join_user, nickname);
    });
    socket.on(SocketEventTypes.message_sent, (message) => {
        winston.log('info', `message "${JSON.stringify(message, null, 2)}" comming`);
        dispatcherClient.emit(SocketEventTypes.message_sent, message);
    });
});

module.exports = {
    start: function() {
        dispatcherClient = ioClient.connect(DispatcherUrl, DispatcherConnectionOpt);
        addHandling();
        http.listen(3100, function(){
            winston.log('info', 'Public API listening on *:3100');
        });
    },
    stop: function(){
        dispatcherClient.close();
        http.close();
    }
};