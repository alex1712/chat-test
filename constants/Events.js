module.exports = {
    SocketEventTypes: {
        connect: 'connect',
        connection: 'connection',
        disconnect: 'disconnect',
        join_user: 'join_user',
        user_joined: 'user_joined',
        leave_user: 'leave_user',
        user_left: 'user_left',
        message_sent: 'message_sent'
    }
};
